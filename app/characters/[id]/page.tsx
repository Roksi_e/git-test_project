"use client";

import React from "react";
import axios from "axios";
import { useQuery } from "@tanstack/react-query";
import Image from "next/image";
import planet7 from "@/components/images/planet-7 5.png";

export async function getDataCharacterById(id: number): Promise<Character> {
  try {
    const res = await axios.get(
      `https://rickandmortyapi.com/api/character/${id}`
    );
    return res.data;
  } catch (error) {
    throw new Error("Failed to fetch data");
  }
}

export default function CharacterPage({ params }: { params: { id: number } }) {
  const {
    data: character,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["character", params.id],
    queryFn: () => getDataCharacterById(params.id),
  });

  if (isLoading) {
    return <h1>Loading</h1>;
  }
  if (isError) {
    return <h1>Oops!!!</h1>;
  }

  return (
    <div className="relative bg-primary w-[80%] h-[60vh] border rounded-lg mx-auto my-16 flex p-8 gap-8 items-center justify-around">
      <div className="w-[60%] h-[90%] relative border border-black rounded-lg overflow-hidden ">
        <Image
          src={
            character?.image
              ? character.image
              : `https://rickandmortyapi.com/api/character/avatar/${params.id}.jpeg`
          }
          alt="character"
          fill
          style={{ objectFit: "cover" }}
        />
      </div>
      <div className="w-[40%] h-[90%] border border-black rounded-lg py-4 px-12 bg-positive">
        <div className="mb-2 text-5xl border-b-2 border-black p-4 w-[80%]">
          {character?.name}
        </div>
        <div className="mb-2">
          <strong>Status:</strong> {character?.status}
        </div>
        <div className="mb-2">
          <strong>Species:</strong> {character?.species}
        </div>
        <div className="mb-2">
          <strong>Type:</strong> {character?.type ? character?.type : `""`}
        </div>
        <div className="mb-2">
          <strong>Gender:</strong> {character?.gender}
        </div>
        <div className="mb-2">
          <strong>Origin:</strong> {character?.origin.name}
        </div>
      </div>
      <div className="w-[200px] h-[200px] border rounded-[50%] absolute right-[-50px] top-[-50px] shadow-[0_0px_47px_15px_rgba(202,250,130,1)]">
        <Image
          src={planet7}
          alt="planet7"
          fill
          style={{ objectFit: "cover" }}
        />
      </div>
    </div>
  );
}
