"use client";

import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import Image from "next/image";
import planet7 from "@/components/images/planet-7 5.png";
import CharacterCard from "@/components/ui/characterCard";
import { getDataCharacterById } from "@/app/characters/[id]/page";

async function getDataPlanetById(id: number): Promise<ILocation> {
  try {
    const res = await axios.get(
      `https://rickandmortyapi.com/api/location/${id}`
    );
    return res.data;
  } catch (error) {
    throw new Error("Failed to fetch data");
  }
}

function Character({ id }: { id: number }) {
  const {
    data: character,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["character", id],
    queryFn: () => getDataCharacterById(id),
  });

  if (isLoading) {
    return <h1>Loading</h1>;
  }
  if (isError) {
    return <h1>Oops!!!</h1>;
  }

  return (
    <>
      {character ? (
        <CharacterCard
          key={character?.id}
          id={character?.id}
          status={character?.status}
          type={character?.type}
          gender={character?.gender}
          species={character?.species}
          image={character?.image}
          name={character?.name}
          origin={character?.origin}
          location={character?.location}
          episode={character?.episode}
          url={character?.url}
          created={character?.created}
        ></CharacterCard>
      ) : null}
    </>
  );
}

export default function LocationPage({ params }: { params: { id: number } }) {
  const {
    data: planet,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["planet", params.id],
    queryFn: () => getDataPlanetById(params.id),
  });

  if (isLoading) {
    return <h1>Loading</h1>;
  }
  if (isError) {
    return <h1>Oops!!!</h1>;
  }

  return (
    <>
      <div className="flex justify-end relative w-full h-[70vh] m-10 p-10">
        <div className="w-[600px] h-[600px] absolute top-0 left-[-200px]">
          <Image
            src={planet7}
            alt="planet7"
            fill
            style={{ objectFit: "cover" }}
          />
        </div>
        <div className="text-white text-4xl w-[60%] h-[100%]">
          <div>
            <strong>Planet:</strong>
            <div className=" mx-24">
              <ul className="list-disc">
                <li>{planet?.name}</li>
              </ul>
            </div>
          </div>
          <div>
            <strong>Type:</strong>
            <div className=" mx-24">
              <ul className="list-disc">
                <li>{planet?.type}</li>
              </ul>
            </div>
          </div>
          <div>
            <strong>Dimension:</strong>
            <div className=" mx-24">
              <ul className="list-disc">
                <li>{planet?.dimension}</li>
              </ul>
            </div>
          </div>
          <div>
            <strong>Created:</strong>
            <div className=" mx-24">
              <ul className="list-disc">
                <li>{planet?.created}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-auto flex flex-row flex-wrap justify-center gap-8 w-[60vw] min-h-80 ">
        {planet?.residents.map((char) => {
          const ky = char.split("/").filter(Boolean);
          const id = parseInt(ky[ky.length - 1]);
          return (
            <div
              key={id}
              className="relative bg-positive rounded p-4 w-64 h-80"
            >
              <Character id={id} key={id}></Character>
            </div>
          );
        })}
      </div>
    </>
  );
}
