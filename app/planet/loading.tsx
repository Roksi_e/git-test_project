export default function LoadingPlanet() {
  return (
    <div className="flex justify-center items-center text-xl text-white relative">
      <h1 className="text-white">Loading...</h1>
    </div>
  );
}
