"use client";

import Image from "next/image";
import Link from "next/link";
import logo from "@/components/images/logo.png";

function Footer() {
  return (
    <footer className="w-full bg-gray-900 h-24 flex items-center gap-4 p-2 z-10">
      <div className="h-[100%] w-1/5 flex items-center justify-center border-r-2 border-white p-2">
        <Image
          src={logo}
          alt="logo"
          style={{ objectFit: "cover" }}
          sizes="w-[100%]"
          quality={100}
        />
      </div>
      <ul className="flex justify-start gap-8 w-4/5 text-white p-2 text-xl">
        <li>
          <Link href="/">Home</Link>
        </li>
        <li>
          <Link href="/favorites">Favorites</Link>
        </li>
        <li>
          <Link href="/characters">Characters</Link>
        </li>
      </ul>
    </footer>
  );
}

export default Footer;
