"use client";

import Link from "next/link";
import Image from "next/image";
import navLogo from "@/components/images/navLogo.png";

const Header = () => {
  return (
    <header className="w-full bg-primary h=[80px] flex justify-end p-2 relative z-10 bg-transparent">
      <div className="h-[100%] w-1/5 flex items-center justify-center  p-2">
        <Image src={navLogo} alt="navlogo" />
      </div>

      <ul className="flex justify-end items-center gap-8 w-4/5 text-green-500 p-2 text-xl">
        <li>
          <Link href="/">Home</Link>
        </li>
        <li>
          <Link href="/favorites">Favorites</Link>
        </li>
        <li>
          <Link href="/characters">Characters</Link>
        </li>
      </ul>
    </header>
  );
};

export default Header;
