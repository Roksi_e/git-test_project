import Link from "next/link";
import Image from "next/image";
import planet from "../images/planet-7 5.png";

export default function PlanetCard({ id, name }: ILocation) {
  return (
    <div className="relative w-[60%] p-2">
      <Link href={`/planet/${id}`}>
        <Image src={planet} alt="planet" />
        <span className="text-white text-sm">{name}</span>
      </Link>
    </div>
  );
}
