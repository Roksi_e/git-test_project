import create from "zustand";

interface AddToFavorite {
  characters: Character[];
  addToFav: (newCharacter: Character) => void;
  deleteToFav: (id: number) => void;
}

export const useAddFavorite = create<AddToFavorite>((set, get) => ({
  characters: [],
  addToFav: (newCharacter) => {
    const { characters } = get();
    set(() => ({ characters: [newCharacter, ...characters] }));
  },

  deleteToFav: (id: number) => {
    const { characters } = get();
    set({ characters: characters.filter((char) => char.id !== id) });
  },
}));
